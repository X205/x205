# phpMyAdmin MySQL-Dump
# version 2.3.2
# http://www.phpmyadmin.net/ (download page)
#
# Host: localhost
# Generation Time: Jan 09, 2003 at 12:37 AM
# Server version: 3.23.54
# PHP Version: 4.2.2
# Database : `xoops2`

#
# Dumping data for table `avatar`
#

INSERT INTO avatar VALUES (1, 'savt3db7aea2d055f.gif', 'Face-001', 'image/gif', 1035447970, 1, 1, 'S');
INSERT INTO avatar VALUES (2, 'savt3db7aeb4ce203.gif', 'Face-002', 'image/gif', 1035447988, 1, 2, 'S');
INSERT INTO avatar VALUES (3, 'savt3db7aecadf020.gif', 'Face-003', 'image/gif', 1035448010, 1, 3, 'S');
INSERT INTO avatar VALUES (4, 'savt3db7aeda1d98f.gif', 'Face-004', 'image/gif', 1035448026, 1, 4, 'S');
INSERT INTO avatar VALUES (5, 'savt3db7aeea068da.gif', 'Face-005', 'image/gif', 1035448042, 1, 5, 'S');
INSERT INTO avatar VALUES (6, 'savt3db7aef775282.gif', 'Face-006', 'image/gif', 1035448055, 1, 6, 'S');
INSERT INTO avatar VALUES (7, 'savt3db7af0559c62.gif', 'Face-007', 'image/gif', 1035448069, 1, 7, 'S');
INSERT INTO avatar VALUES (8, 'savt3db7af160295d.gif', 'Face-008', 'image/gif', 1035448086, 1, 8, 'S');
INSERT INTO avatar VALUES (9, 'savt3db7af23bdb0c.gif', 'Face-009', 'image/gif', 1035448099, 1, 9, 'S');
INSERT INTO avatar VALUES (10, 'savt3db7af34ac05e.gif', 'Face-010', 'image/gif', 1035448116, 1, 10, 'S');
INSERT INTO avatar VALUES (11, 'savt3db7af428f2c6.gif', 'Face-011', 'image/gif', 1035448130, 1, 11, 'S');
INSERT INTO avatar VALUES (12, 'savt3db7af4f14f7f.gif', 'Face-012', 'image/gif', 1035448143, 1, 12, 'S');
INSERT INTO avatar VALUES (13, 'savt3db7af60bd03e.gif', 'Face-013', 'image/gif', 1035448160, 1, 13, 'S');
INSERT INTO avatar VALUES (14, 'savt3db7af6da45ad.gif', 'Face-014', 'image/gif', 1035448173, 1, 14, 'S');
INSERT INTO avatar VALUES (15, 'savt3db7af7b12cee.gif', 'Face-015', 'image/gif', 1035448187, 1, 15, 'S');
INSERT INTO avatar VALUES (16, 'savt3db7af86df0ef.gif', 'Face-016', 'image/gif', 1035448198, 1, 16, 'S');
INSERT INTO avatar VALUES (17, 'savt3db7af947f0ab.gif', 'Face-017', 'image/gif', 1035448212, 1, 17, 'S');
INSERT INTO avatar VALUES (18, 'savt3db7afa2864c3.gif', 'Face-018', 'image/gif', 1035448226, 1, 18, 'S');
INSERT INTO avatar VALUES (19, 'savt3db7afb021c3c.gif', 'Face-019', 'image/gif', 1035448240, 1, 19, 'S');
INSERT INTO avatar VALUES (20, 'savt3db7afc1e013e.gif', 'Face-020', 'image/gif', 1035448258, 1, 20, 'S');
INSERT INTO avatar VALUES (21, 'savt3db7afcf2a9d4.gif', 'Face-021', 'image/gif', 1035448271, 1, 21, 'S');
INSERT INTO avatar VALUES (22, 'savt3db7afda2d2f3.gif', 'Face-022', 'image/gif', 1035448282, 1, 22, 'S');
INSERT INTO avatar VALUES (23, 'savt3db7b0019a02d.gif', 'Face-023', 'image/gif', 1035448321, 1, 23, 'S');
INSERT INTO avatar VALUES (24, 'savt3db7b00eb0df3.gif', 'Face-024', 'image/gif', 1035448334, 1, 24, 'S');
INSERT INTO avatar VALUES (25, 'savt3db7b019d50ae.gif', 'Face-025', 'image/gif', 1035448345, 1, 25, 'S');
INSERT INTO avatar VALUES (26, 'savt3db7b025bf39d.gif', 'Face-026', 'image/gif', 1035448357, 1, 26, 'S');
INSERT INTO avatar VALUES (27, 'savt3db7b032788b6.gif', 'Face-027', 'image/gif', 1035448370, 1, 27, 'S');
INSERT INTO avatar VALUES (28, 'savt3db7b03e39efa.gif', 'Face-028', 'image/gif', 1035448382, 1, 28, 'S');
INSERT INTO avatar VALUES (29, 'savt3db7b04b9b5b0.gif', 'Face-029', 'image/gif', 1035448395, 1, 29, 'S');
INSERT INTO avatar VALUES (30, 'savt3db7b05c08d7b.gif', 'Face-030', 'image/gif', 1035448412, 1, 30, 'S');

#
# Dumping data for table `avatar_user_link`
#


#
# Dumping data for table `bannerclient`
#

INSERT INTO bannerclient VALUES (0, 'XOOPS', 'XOOPS Dev Team', 'webmaster@xoops.org', '', '', '');

#
# Dumping data for table `bannerfinish`
#


#
# Dumping data for table `comments`
#


#
# Dumping data for table `configcategory`
#

INSERT INTO configcategory VALUES (1, '_MD_AM_GENERAL', 0);
INSERT INTO configcategory VALUES (2, '_MD_AM_USERSETTINGS', 0);
INSERT INTO configcategory VALUES (3, '_MD_AM_METAFOOTER', 0);
INSERT INTO configcategory VALUES (4, '_MD_AM_CENSOR', 0);
INSERT INTO configcategory VALUES (5, '_MD_AM_SEARCH', 0);
INSERT INTO configcategory VALUES (6, '_MD_AM_MAILER', 0);

#
# Dumping data for table `configoption`
#

INSERT INTO configoption VALUES (1, '_MD_AM_DEBUGMODE1', '1', 13);
INSERT INTO configoption VALUES (2, '_MD_AM_DEBUGMODE2', '2', 13);
INSERT INTO configoption VALUES (3, '_NESTED', 'nest', 32);
INSERT INTO configoption VALUES (4, '_FLAT', 'flat', 32);
INSERT INTO configoption VALUES (5, '_THREADED', 'thread', 32);
INSERT INTO configoption VALUES (6, '_OLDESTFIRST', '0', 33);
INSERT INTO configoption VALUES (7, '_NEWESTFIRST', '1', 33);
INSERT INTO configoption VALUES (8, '_MD_AM_USERACTV', '0', 21);
INSERT INTO configoption VALUES (9, '_MD_AM_AUTOACTV', '1', 21);
INSERT INTO configoption VALUES (10, '_MD_AM_ADMINACTV', '2', 21);
INSERT INTO configoption VALUES (11, '_MD_AM_STRICT', '0', 23);
INSERT INTO configoption VALUES (12, '_MD_AM_MEDIUM', '1', 23);
INSERT INTO configoption VALUES (13, '_MD_AM_LIGHT', '2', 23);
INSERT INTO configoption VALUES (14, '_MD_AM_DEBUGMODE3', '3', 13);
INSERT INTO configoption VALUES (15, '_MD_AM_INDEXFOLLOW', 'index,follow', 43);
INSERT INTO configoption VALUES (16, '_MD_AM_NOINDEXFOLLOW', 'noindex,follow', 43);
INSERT INTO configoption VALUES (17, '_MD_AM_INDEXNOFOLLOW', 'index,nofollow', 43);
INSERT INTO configoption VALUES (18, '_MD_AM_NOINDEXNOFOLLOW', 'noindex,nofollow', 43);
INSERT INTO configoption VALUES (19, '_MD_AM_METAOGEN', 'general', 48);
INSERT INTO configoption VALUES (20, '_MD_AM_METAO14YRS', '14 years', 48);
INSERT INTO configoption VALUES (21, '_MD_AM_METAOREST', 'restricted', 48);
INSERT INTO configoption VALUES (22, '_MD_AM_METAOMAT', 'mature', 48);
INSERT INTO configoption VALUES (23, '_MD_AM_DEBUGMODE0', '0', 13);

INSERT INTO configoption VALUES (24,'PHP mail()','mail',64);
INSERT INTO configoption VALUES (25,'sendmail','sendmail',64);
INSERT INTO configoption VALUES (26,'SMTP','smtp',64);
INSERT INTO configoption VALUES (27,'SMTPAuth','smtpauth',64);



#
# Dumping data for table `image`
#


#
# Dumping data for table `imagebody`
#


#
# Dumping data for table `imagecategory`
#


#
# Dumping data for table `imgset`
#

INSERT INTO imgset VALUES (1, 'default', 0);

#
# Dumping data for table `imgset_tplset_link`
#

INSERT INTO imgset_tplset_link VALUES (1, 'default');

#
# Dumping data for table `online`
#


#
# Dumping data for table `priv_msgs`
#


#
# Dumping data for table `session`
#
