function xoopsGetElementById(id){
	if (document.getElementById) {
		return (document.getElementById(id));
	} else if (document.all) {
		return (document.all[id]);
	} else {
		if ((navigator.appname.indexOf("Netscape") != -1) && parseInt(navigator.appversion == 4)) {
			return (document.layers[id]);
		}
	}
}

function showImgSelected(imgId, selectId, imgDir, extra) {
	imgDom = xoopsGetElementById(imgId);
	selectDom = xoopsGetElementById(selectId);
	imgDom.src = "<?php echo XOOPS_URL;?>/"+ imgDir + "/" + selectDom.options[selectDom.selectedIndex].value + extra;
}

function justReturn() {
	return;
}

function init() {

}

function openWithSelfMain(url,name,width,height) {
	var options = "width=" + width + ",height=" + height + "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no";

	new_window = window.open(url, name, options);
	window.self.name = "main";
	new_window.focus();
}

function setElementColor(id, color){
	xoopsGetElementById(id).style.color = "#" + color;
}

function setElementFont(id, font){
	xoopsGetElementById(id).style.fontFamily = font;
}

function setElementSize(id, size){
	xoopsGetElementById(id).style.fontSize = size;
}

function changeDisplay(id){
	var elestyle = xoopsGetElementById(id).style;
	if (elestyle.display == "") {
		elestyle.display = "none";
	} else {
		elestyle.display = "block";
	}
}

function setVisible(id){
	xoopsGetElementById(id).style.visibility = "visible";
}

function setHidden(id){
	xoopsGetElementById(id).style.visibility = "hidden";
}

function makeBold(id){
	var eleStyle = xoopsGetElementById(id).style;
	if (eleStyle.fontWeight != "bold") {
		eleStyle.fontWeight = "bold";
	} else {
		eleStyle.fontWeight = "normal";
	}
}

function makeItalic(id){
	var eleStyle = xoopsGetElementById(id).style;
	if (eleStyle.fontStyle != "italic") {
		eleStyle.fontStyle = "italic";
	} else {
		eleStyle.fontStyle = "normal";
	}
}

function makeUnderline(id){
	var eleStyle = xoopsGetElementById(id).style;
	if (eleStyle.textDecoration != "underline") {
		eleStyle.textDecoration = "underline";
	} else {
		eleStyle.textDecoration = "none";
	}
}

function makeLineThrough(id){
	var eleStyle = xoopsGetElementById(id).style;
	if (eleStyle.textDecoration != "line-through") {
		eleStyle.textDecoration = "line-through";
	} else {
		eleStyle.textDecoration = "none";
	}
}


function appendSelectOption(selectMenuId, optionName, optionValue){
	var selectMenu = xoopsGetElementById(selectMenuId);
	var newoption = new Option(optionName, optionValue);
	selectMenu.options[selectMenu.length] = newoption;
	selectMenu.options[selectMenu.length].selected = true;
}

function disableElement(target){
	var targetDom = xoopsGetElementById(target);
	if (targetDom.disabled != true) {
		targetDom.disabled = true;
	} else {
		targetDom.disabled = false;
	}
}
function xoopsCheckAll(formname, switchid) {
	var ele = document.forms[formname].elements;
	var switch_cbox = xoopsGetElementById(switchid);
	for (var i = 0; i < ele.length; i++) {
		var e = ele[i];
		if ( (e.name != switch_cbox.name) && (e.type == 'checkbox') ) {
			e.checked = switch_cbox.checked;
		}
	}
}

// RMV-NOTIFY
function xoopsCheckGroup(formname, switchid, groupid) {
	var ele = document.forms[formname].elements;
	var switch_cbox = xoopsGetElementById(switchid);
	for (var i = 0; i < ele.length; i++) {
		var e = ele[i];
		if ( (e.type == 'checkbox') && (e.id == groupid) ) {
			e.checked = switch_cbox.checked;
			e.click(); e.click();  // Click to activate subgroups
									// Twice so we don't reverse effect
		}
	}
}

function xoopsCodeUrl(id){
	var text = prompt("<?php echo _ENTERURL;?>", "");
	var domobj = xoopsGetElementById(id);
	if ( text != null && text != "" ) {
		var text2 = prompt("<?php echo _ENTERWEBTITLE;?>", "");
		if ( text2 != null ) {
			if ( text2 == "" ) {
				var result = "[url=" + text + "]" + text + "[/url]";
			} else {
				var pos = text2.indexOf(unescape('%00'));
				if(0 < pos){
					text2 = text2.substr(0,pos);
				}
				var result = "[url=" + text + "]" + text2 + "[/url]";
			}
			xoopsInsertText(domobj, result);
		}
	}
	domobj.focus();
}

function xoopsCodeImg(id){
	var text = prompt("<?php echo _ENTERIMGURL;?>", "");
	var domobj = xoopsGetElementById(id);
	if ( text != null && text != "" ) {
		var text2 = prompt("<?php echo _ENTERIMGPOS;?>\n<?php echo _IMGPOSRORL;?>", "");
		while ( ( text2 != "" ) && ( text2 != "r" ) && ( text2 != "R" ) && ( text2 != "l" ) && ( text2 != "L" ) && ( text2 != null ) ) {
			text2 = prompt("<?php echo _ERRORIMGPOS;?>\n<?php echo _IMGPOSRORL;?>","");
		}
		if ( text2 == "l" || text2 == "L" ) {
			text2 = " align=left";
		} else if ( text2 == "r" || text2 == "R" ) {
			text2 = " align=right";
		} else {
			text2 = "";
		}
		var result = "[img" + text2 + "]" + text + "[/img]";
		xoopsInsertText(domobj, result);
	}
	domobj.focus();
}

function xoopsCodeEmail(id){
	var text = prompt("<?php echo _ENTEREMAIL;?>", "");
	var domobj = xoopsGetElementById(id);
	if ( text != null && text != "" ) {
		var result = "[email]" + text + "[/email]";
		xoopsInsertText(domobj, result);
	}
	domobj.focus();
}

function xoopsCodeQuote(id){
	var text = prompt("<?php echo _ENTERQUOTE;?>", "");
	var domobj = xoopsGetElementById(id);
	if ( text != null && text != "" ) {
		var pos = text.indexOf(unescape('%00'));
		if(0 < pos){
			text = text.substr(0,pos);
		}
		var result = "[quote]" + text + "[/quote]";
		xoopsInsertText(domobj, result);
	}
	domobj.focus();
}

function xoopsCodeCode(id){
	var text = prompt("<?php echo _ENTERCODE;?>", "");
	var domobj = xoopsGetElementById(id);
	if ( text != null && text != "" ) {
		var result = "[code]" + text + "[/code]";
		xoopsInsertText(domobj, result);
	}
	domobj.focus();
}



function xoopsCodeText(id, hiddentext){
	var textareaDom = xoopsGetElementById(id);
	var textDom = xoopsGetElementById(id + "Addtext");
	var fontDom = xoopsGetElementById(id + "Font");
	var colorDom = xoopsGetElementById(id + "Color");
	var sizeDom = xoopsGetElementById(id + "Size");
	var xoopsHiddenTextDomStyle = xoopsGetElementById(hiddentext).style;
	var textDomValue = textDom.value;
	var fontDomValue = fontDom.options[fontDom.options.selectedIndex].value;
	var colorDomValue = colorDom.options[colorDom.options.selectedIndex].value;
	var sizeDomValue = sizeDom.options[sizeDom.options.selectedIndex].value;
	if ( textDomValue == "" ) {
		alert("<?php echo _ENTERTEXTBOX;?>");
		textDom.focus();
	} else {
		if ( fontDomValue != "FONT") {
			textDomValue = "[font=" + fontDomValue + "]" + textDomValue + "[/font]";
			fontDom.options[0].selected = true;
		}
		if ( colorDomValue != "COLOR") {
			textDomValue = "[color=" + colorDomValue + "]" + textDomValue + "[/color]";
			colorDom.options[0].selected = true;
		}
		if ( sizeDomValue != "SIZE") {
			textDomValue = "[size=" + sizeDomValue + "]" + textDomValue + "[/size]";
			sizeDom.options[0].selected = true;
		}
		if (xoopsHiddenTextDomStyle.fontWeight == "bold") {
			textDomValue = "[b]" + textDomValue + "[/b]";
			xoopsHiddenTextDomStyle.fontWeight = "normal";
		}
		if (xoopsHiddenTextDomStyle.fontStyle == "italic") {
			textDomValue = "[i]" + textDomValue + "[/i]";
			xoopsHiddenTextDomStyle.fontStyle = "normal";
		}
		if (xoopsHiddenTextDomStyle.textDecoration == "underline") {
			textDomValue = "[u]" + textDomValue + "[/u]";
			xoopsHiddenTextDomStyle.textDecoration = "none";
		}
		if (xoopsHiddenTextDomStyle.textDecoration == "line-through") {
			textDomValue = "[d]" + textDomValue + "[/d]";
			xoopsHiddenTextDomStyle.textDecoration = "none";
		}
		xoopsInsertText(textareaDom, textDomValue);
		textDom.value = "";
		xoopsHiddenTextDomStyle.color = "#000000";
		xoopsHiddenTextDomStyle.fontFamily = "";
		xoopsHiddenTextDomStyle.fontSize = "12px";
		xoopsHiddenTextDomStyle.visibility = "hidden";
		textareaDom.focus();
	}
}

function xoopsSavePosition(id)
{
	var textareaDom = xoopsGetElementById(id);
	if (textareaDom.createTextRange) {
		textareaDom.caretPos = document.selection.createRange().duplicate();
	}
}

function xoopsInsertText(domobj, text)
{
	if (domobj.createTextRange && domobj.caretPos){
  		var caretPos = domobj.caretPos;
		caretPos.text = caretPos.text.charAt(caretPos.text.length - 1) 
== ' ' ? text + ' ' : text;  
	} else if (domobj.getSelection && domobj.caretPos){
		var caretPos = domobj.caretPos;
		caretPos.text = caretPos.text.charat(caretPos.text.length - 1)  
== ' ' ? text + ' ' : text;
	} else {
		domobj.value = domobj.value + text;
  	}
}

function xoopsCodeSmilie(id, smilieCode) {
	var revisedMessage;
	var textareaDom = xoopsGetElementById(id);
	xoopsInsertText(textareaDom, smilieCode);
	textareaDom.focus();
	return;
}

function xoopsValidate(subjectId, textareaId, submitId) {
	var maxchars = 65535;
	var subjectDom = xoopsGetElementById(subjectId);
	var textareaDom = xoopsGetElementById(textareaId);
	var submitDom = xoopsGetElementById(submitId);
	if (textareaDom.value == "" || subjectDom.value == "") {
		alert("<?php echo _PLZCOMPLETE;?>");
		return false;
	}
	if (maxchars != 0) {
		if (textareaDom.value.length > maxchars) {
			alert("<?php echo _MESSAGETOOLONG;?>\n\n<?php echo _ALLOWEDCHAR;?>" + maxchars + "\n<?php echo _CURRCHAR;?>" + textareaDom.value.length + "");
			textareaDom.focus();
			return false;
		} else {
			submitDom.disabled = true;
			return true;
		}
	} else {
		submitDom.disabled = true;
		return true;
	}
}
window.onload=init;
