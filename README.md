# x205

# x205 CMS

I have decided to rebuild [XOOPS 2.0.5]() to turn into the ***x205 CMS***. I believe it was last updated in 2005 (maybe the begining of 2006), we now are ending 2018. Things have changed a lot since then. But that was the last toy I really liked playing with.

So my plans are to get [XOOPS 2.0.5]() working as close as it did back then, but with these

## Features
* PHP 7+
* MySQLi and/or PDO





Other things will happen as we go along in development, so keep an eye on the [ISSUES page](https://gitlab.com/X205/x205/issues)

and sometimes ...

... there may be some development info inside the [wiki](https://gitlab.com/X205/x205/wikis/home).


### Planned is

* To refactor the project to be closer to an MVC method.
* We need to have a method of online updating, simular to wordpress would be great.
* Page integration, such as legal pages. 
* Adding things into the that WILL BE NEEDED ON ANY TYPE OF SITE.
* A different method of translating, 
    * instead of defining a languange, use *.po & *.mo files 